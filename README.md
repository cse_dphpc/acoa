# Ant Colony Optimization Algorithms

*developed by [Marc Wanner](mailto:wannerm@ethz.ch), [Aurelio Dolfini](mailto:adolfini@ethz.ch), [Dominik Helmreich](mailto:hedomini@ethz.ch) and [Jan Stratmann](mailto:strjan@ethz.ch)*

## About

This repository contains code to test an ACO heuristic on the Symmetric Travelling Salesman Problem (TSP). It was written in order to apply the contents taught in the Course "Design of Parallel and High-Performance Computing" by Markus Pueschel and Tal Ben-Nun at [ETH Zurich](https://ethz.ch/en.html). For details please see the [course webpage](http://spcl.inf.ethz.ch/Teaching/2019-dphpc/). The repository also contains the project report, which summarizes our testing results and explains the applied concepts in detail.
The code can be used to build five programs: A sequential version and four parallel versions (using OpenMP, MPI, both, and manual threading). Each version can be applied to any symmetric TSP instance of the [TSPLib](http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/index.html) using the `EUC_2D` edge weight type.

## Installation

In order to test our program, you should have [CMake](https://cmake.org), the [OpenMP](https://www.openmp.org) library, [MPI](https://www.open-mpi.org) and [GCC](https://gcc.gnu.org) installed (Clang will work fine for the sequential and OpenMP version, but hasn't been tested when using MPI).
We also include scripts to run jobs on a cluster using the LSF batch system. However, these are tailored to ETH's [Euler Cluster](https://scicomp.ethz.ch/wiki/Euler) and may need some modification to work on your system.

Once you have downloaded the repo and installed the above dependencies, running your first job should be as simple as typing `bash run.sh` inside the `Cpp` folder. Don't forget to set the CMake build type to 'Release' before benchmarking! Please reach out to us if you are experiencing trouble.

If you want to use our plotting scripts, you will also need a working python installation and might need to install some additional packages depending on you setup.

## Using the program

TODO: add description for every parameter, document file hierarchy and plotting

Inside the `Python` folder, you will find our inital python version. It is not very sophisticated, but might be interesting to users new to the program.
The more interesting stuff happens in the `Cpp` folder: Here, you will find drivers (called `main_`_method_`.cpp`) and running scripts (`run_`_method_`.sh`). If you want to use an LSF batch system, you can find some inspiration in the `euler_`_method_`.sh` files, which are currently tailored to use ETH's Euler cluster.
Here's a quick _method_ guide:
- seq: The original sequential version
- mpi: Parallel version using only MPI
- omp: Parallel version using only OMP
- mpi_omp: A hybrid parallel version combining MPI and OMP
- groupthread: Parallel version using manual threading

## Design Guidelines

1. Variable Names are all lowercase and connected by underscores, e.g. `variable_name`
2. Functions are almost the same: please use Capital letters instead, e.g. `Function_Name`
3. Try to use type aliases as much as (reasonably) possible, so we can e.g. switch between doubles and floats later
4. Make use of `static`, `const`, `using`, etc.
5. Document your code well!

## Goals

1. Implementation of sequential ACOA
2. Thread-Parallel version
3. MPI Version (time allowing)
