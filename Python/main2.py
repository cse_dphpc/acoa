import numpy as np
import copy as cp
import random


# ----------1.set parameters-----------
m=8 #nr nodes
n=10 #nr ants
iterations=500 #max iteration, eary stopping by confirmation_threshold!
confirmation_th=0.95

initial_pheromone=0.1 #initial value and lower bound
upper_b_pheromone=1.5 #initial value and lower bound
q=10 #const for pheromone update
r=0.5 #pheromone evaporation

alpha=2 #for transition probabilities
beta=2 #for transition probabilities

#nodes / cities, first=start&end
nodes=[(5,10),(8,8),(10,5),(8,2),(5,0),(2,2),(0,5),(2,8)]
# nodes=[(5,10),(8,8),(10,5),(10,2),(8,2),(8,0),(5,0),(2,2),(0,5),(2,8)]



# ----------2.set public datastructures-----------
def rs_dist(a, b):
    dist=(a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])
    return np.sqrt(dist)

distances=[]
for i in range(len(nodes)):
    row=[]
    for j in range(len(nodes)):
        row.append(rs_dist(nodes[i], nodes[j]))
    distances.append(row)

pheromones=[]
for i in range(len(nodes)):
    row=[]
    for j in range(len(nodes)):
        row.append(initial_pheromone)
    pheromones.append(row)

shortest_path=[]

shortest_dist=0
for i in range(len(nodes)): #to make sure the distance is big enough
    for j in range(len(nodes)):
        shortest_dist+=distances[i][j]

shortest_ctr=0

total_paths=0

# ----------3.simulation-----------

# ----------3.1 functions-----------
def pheromones_decrease(pheromones, r):
    for i in range(len(nodes)):
        for j in range(len(nodes)):
            pheromones[i][j]*=(1-r)

def pheromones_increase(pheromones, visited_edges, distance, q):
    increase=q/distance
    # print("INCREASE "+str(increase))
    for i in range(len(nodes)):
        for j in range(len(nodes)):
            if visited_edges[i][j]==1:
                pheromones[i][j]+=increase

def trans_prob(distances, pheromones, visited_nodes, alpha, beta, edge): #not normped!
    if visited_nodes[edge[1]]==1:
        return 0
    else:
        # print("PHERO: "+str(np.power(pheromones[edge[0]][edge[1]], alpha))+" DIST: "+str(np.power(1/distances[edge[0]][edge[1]], beta)))
        return np.power(pheromones[edge[0]][edge[1]], alpha) * np.power(1/distances[edge[0]][edge[1]], beta)

def all_nodes(visited_nodes):
    for i in range(len(nodes)):
        if visited_nodes[i]==0:
            return False
    return True

# def make_undirected(a, b):
#     if b<a:
#         return b,a
#     else:
#         return a,b


# ----------3.2 iteration rounds-----------
for it in range(iterations):
    print("---------------Iteration "+str(it)+"---------------")
    new_pheromones=cp.deepcopy(pheromones) #new pheromones, active after complete iteration
    pheromones_decrease(new_pheromones, r)

    # ----------3.3 iteration over ants-----------
    for ant in range(n):
        print("-----ANT "+str(ant)+"-----")
        ant_pos=0 #P1. position

        ant_visited_nodes=[] #P2. visited nodes
        for i in range(len(nodes)):
            ant_visited_nodes.append(0)
        ant_visited_nodes[0]=1

        ant_path = [] #P3.
        ant_path.append(0)
        ant_path_length=0 #P4. path lenght

        ant_visited_edges=[] #P5. visited edges
        for i in range(len(nodes)):
            row=[]
            for j in range(len(nodes)):
                row.append(0)
            ant_visited_edges.append(row)

        # ----------3.4 searching path-----------
        while(not all_nodes(ant_visited_nodes)):
            prob=[] #calculate prob for next nodes
            tot_prob=0
            for next in range(len(nodes)):
                p=trans_prob(distances, pheromones, ant_visited_nodes, alpha, beta, (ant_pos,next))
                prob.append(p)
                tot_prob+=p
            for i in range(len(prob)):
                prob[i]/=tot_prob
            # print("PROB: "+str(prob))
            # print(pheromones)
            next=np.random.choice(np.arange(0, len(nodes), 1), p=prob)

            ant_path.append(next)
            ant_visited_edges[ant_pos][next]=1 #update ant data
            ant_path_length+=distances[ant_pos][next]
            ant_visited_nodes[next]=1

            print(next,end="  ",flush=True)
            ant_pos=next
            # print("POS: "+str(ant_pos))
            # print("PATH LENGHT: "+str(ant_path_length))
            # print("VISITED NODES: "+str(ant_visited_nodes))
            # print("VISITED EDGES: "+str(ant_visited_edges))

        ant_path.append(0)
        ant_visited_edges[ant_pos][0]=1 #back to start&end node!
        ant_path_length+=distances[ant_pos][0]
        ant_pos=0

        # print("POS: "+str(ant_pos))
        print("PATH LENGHT: "+str(ant_path_length))
        # print("VISITED NODES: "+str(ant_visited_nodes))
        # print("VISITED EDGES: "+str(ant_visited_edges))

        pheromones_increase(new_pheromones, ant_visited_edges, ant_path_length, q) #update NEW_pheromones

        #----update best performance----
        if ant_path_length == shortest_dist:
            shortest_ctr+=1
            total_paths+=1
            print("SHORTEST CONFIRMED: "+str(shortest_dist))
        elif ant_path_length < shortest_dist:
            shortest_path=ant_path
            shortest_dist=ant_path_length
            shortest_ctr=1
            total_paths+=1
            print("SHORTEST UPDATED: "+str(shortest_dist))
        else:
            total_paths+=1
            print("WORSE")

        # print(shortest_path) #--
        # print(shortest_dist) #--
        # print(shortest_ctr) #--
        # print(total_paths) #--


    pheromones=cp.deepcopy(new_pheromones) #activate new_pheromones
    for i in range(len(nodes)): #impose uper and lower bounds
        for j in range(len(nodes)):
            if pheromones[i][j]<initial_pheromone:
                pheromones[i][j]=initial_pheromone
            if pheromones[i][j]>upper_b_pheromone:
                pheromones[i][j]=upper_b_pheromone

    print("UPDATED PHEROMONES:")
    print(pheromones)

    print("SUMMARY:" + "Shortest Path So Far: " + str(shortest_path) + " ->Dist: " + str(shortest_dist) + " With Confirmation: " +str(shortest_ctr) + "/" + str(total_paths))
    if shortest_ctr/total_paths>=confirmation_th:
        exit()



