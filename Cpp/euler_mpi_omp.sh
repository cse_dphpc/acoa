#!/bin/bash

MACHINE="Euler_Hybrid_2Ranks"
N=10 #number of tests on a single case


#load appropriate cmake module
module load cmake/3.5.2
module load open_mpi/1.6.5

#compile program
cmake -Bbuild -H./ -DCMAKE_BUILD_TYPE=Release
make -C build

#put loop here if you want to benchmark multiple test cases
FILE="kroA100"

INPUTFILENAME="Test_Cases/$FILE.in"
OUTFILE="Test_Cases/$FILE.out"
CSVFILE="Test_Cases/$FILE.csv"
PATHFILE="Test_Cases/$FILE.path"

if [ -f $CSVFILE ]
then
	touch $CSVFILE
fi

if [ -f $PATHFILE ]
then
	touch $PATHFILE
fi

#benchmarking on a single test case
LOWER_PHERO_BOUND=0.0001
UPPER_PHERO_BOUND=100.0
ALPHA=1
BETA=3
R=0.05
Q=10
NUMANTSMAX=2
ITERATIONS=5000 #FUNKTIONIERT NOCH NICHT!! auch processes
PROCESSES=2 #MPI
THREADS=3 #OPEN_MP
TOTALCORES=6 #set to PROCESSES * THREADS

for NUMANTS in {12,32}
do
	JOB="mpirun -np $PROCESSES --cpus-per-proc $THREADS ./build/acoa_mpi_omp $INPUTFILENAME $MACHINE $LOWER_PHERO_BOUND $UPPER_PHERO_BOUND $ALPHA $BETA $R $Q $NUMANTS $ITERATIONS $TOTALCORES $THREADS > $OUTFILE"

	echo "Submitting $N jobs with INPUTFILENAME=$INPUTFILENAME, MACHINE=$MACHINE, LOWER_PHERO_BOUND=$LOWER_PHERO_BOUND, UPPER_PHERO_BOUND=$UPPER_PHERO_BOUND, ALPHA=$ALPHA, BETA=$BETA, R=$R, Q=$Q, NUMANTS=$NUMANTS, ITERATIONS=$ITERATIONS PROCESSES=$PRC THREADS=$THREADS..."
	export OMP_NUM_THREADS=$THREADS
	bsub -W 00:10 -n $TOTALCORES -J "$FILE[1-$N]" -R "rusage[mem=512]" -r "$JOB"
done
