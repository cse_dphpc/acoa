#!/bin/bash

#Put your name below instead of Local
MACHINE="Aurelio_groupthread" #also add version (seq, mpi, omp, mpi_omp, groupthread)
FILE="bier127"
Plotting="False" #True or False
ImgType="png" #png or eps or ...

INPUTFILENAME="Test_Cases/$FILE.in"
LOWER_PHERO_BOUND=0.001
UPPER_PHERO_BOUND=10.0
ALPHA=2
BETA=2
R=0.1
Q=10
NUMANTS=10
ITERATIONS=333
NUMTHREADS=4

OUTFILE="Test_Cases/$FILE.out"

CSVFILE="Test_Cases/$FILE.csv"
if [ -f $CSVFILE ]
then
	touch $CSVFILE
fi

PATHFILE="Test_Cases/$FILE.path"
if [ -f $PATHFILE ]
then
	touch $PATHFILE
fi

echo "-----Building-----"
cmake -Bbuild -H./
make -C build

echo "-----Running ($FILE)-----"
echo "...with INPUTFILENAME=$INPUTFILENAME, MACHINE=$MACHINE, LOWER_PHERO_BOUND=$LOWER_PHERO_BOUND, UPPER_PHERO_BOUND=$UPPER_PHERO_BOUND, ALPHA=$ALPHA, BETA=$BETA, R=$R, Q=$Q, NUMANTS=$NUMANTS, ITERATIONS=$ITERATIONS NUMTHREADS=$NUMTHREADS ..."
time ./build/acoa_groupthread $INPUTFILENAME $MACHINE $LOWER_PHERO_BOUND $UPPER_PHERO_BOUND $ALPHA $BETA $R $Q $NUMANTS $ITERATIONS $NUMTHREADS > $OUTFILE

if [ "$Plotting" == "True" ]; then
    echo $"-----Plot shortest_path.py ($FILE)-----"
    python3 Plotting/shortest_path.py $FILE $ImgType
    echo $"-----Plot seq_stacked_bar_machines.py ($FILE)-----"
    python3 Plotting/seq_stacked_bar_machines.py $FILE $ImgType
fi
