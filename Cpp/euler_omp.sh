#!/bin/bash

MACHINE="Euler_OMP"
N=10 #number of tests on a single case


#load appropriate cmake module
module load cmake/3.5.2

#compile program
cmake -Bbuild -H./ -DCMAKE_BUILD_TYPE=Release
make -C build

#put loop here if you want to benchmark multiple test cases
FILE="kroA100"

INPUTFILENAME="Test_Cases/$FILE.in"
OUTFILE="Test_Cases/$FILE.out"
CSVFILE="Test_Cases/$FILE.csv"
PATHFILE="Test_Cases/$FILE.path"

if [ -f $CSVFILE ]
then
	touch $CSVFILE
fi

if [ -f $PATHFILE ]
then
	touch $PATHFILE
fi
if [ -f $OUTFILE ]
then 
	touch $OUTFILE
fi

#benchmarking on a single test case
LOWER_PHERO_BOUND=0.0001
UPPER_PHERO_BOUND=100.0
ALPHA=1
BETA=3
R=0.05
Q=10
ITERATIONS=5000

	for NUMTHREADS in {1,2,3,4,5,6}
do
		for NUMANTS in {12,32}
	do
		JOB="time ./build/acoa_omp $INPUTFILENAME $MACHINE $LOWER_PHERO_BOUND $UPPER_PHERO_BOUND $ALPHA $BETA $R $Q $NUMANTS $ITERATIONS > $OUTFILE"

		echo "Submitting $N jobs with INPUTFILENAME=$INPUTFILENAME, MACHINE=$MACHINE, LOWER_PHERO_BOUND=$LOWER_PHERO_BOUND, UPPER_PHERO_BOUND=$UPPER_PHERO_BOUND, ALPHA=$ALPHA, BETA=$BETA, R=$R, Q=$Q, NUMANTS=$NUMANTS, ITERATIONS=$ITERATIONS ..."
		export OMP_NUM_THREADS=$NUMTHREADS
		bsub -W 0:02 -J "$FILE[1-$N]" -R "rusage[mem=512] select[model=XeonGold_5118]" -n $NUMTHREADS -r "$JOB"
	done
done
