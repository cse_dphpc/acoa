#!/bin/bash
#Plotting of Euler Data

#Choose input
FILE="kroA100"
ImgType="png" #png or eps or ...

NUMANTS="12"

echo $"-----BASH SCRIPT EVENTUALLY NOT UPDATED-----"

echo $"-----Plot seg_ants_per_it.py ($FILE, $NUMANTS ants)-----"
python3 Plotting/seq_ants_per_it.py $FILE $ImgType
echo $"-----Plot nr_threads.py ($FILE, $NUMANTS ants)-----"
python3 Plotting/nr_threads.py $FILE $ImgType

echo $"-----Plot nr_threads_speedup.py ($FILE, $NUMANTS ants)-----"
python3 Plotting/nr_threads_speedup_new.py $FILE $NUMANTS $ImgType
echo $"-----Plot nr_threads_speedup_box.py ($FILE, $NUMANTS ants)-----"
python3 Plotting/nr_threads_speedup_box_new.py $FILE $NUMANTS $ImgType
echo $"-----Plot nr_threads_speedup_PRAM.py ($FILE, $NUMANTS ants)-----"
python3 Plotting/nr_threads_speedup_new_new.py $FILE $NUMANTS $ImgType
