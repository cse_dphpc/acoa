versions: seq, mpi, omp, mpi_omp, groupthread
-each has own main_version.cpp
-each has own run_version.sh
-each has own euler_version.sh

-all based on same src/*
-all constructed in same CMake -> message "run cmake version"
-all write into same Testcases/file.csv so make sure Machine-info in bash includes version

remark: maybe you have to set your correct mpi command correct in the run_version.sh (mpirun.mpich or mpiexec or..)
