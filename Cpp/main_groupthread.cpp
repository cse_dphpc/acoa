/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <limits>
#include <algorithm>
#include <iomanip>
#include <thread>

#include "src/ant.hpp"
#include "timer/timer.hpp"

int main(int argc, const char** argv) {
    Timer init, main_loop, ant_run, bound_update, update, send, receive, total,
            termination, single_thread, thread_join, thread_init;
    double ant_run_total = 0, update_total = 0, send_total = 0, receive_total = 0,
            bound_update_total = 0, termination_total = 0, single_thread_total = 0,
            thread_join_total = 0, thread_init_total = 0;

    /*TIME CHECKPOINT*/
    total.start();
    init.start();
    /*TIME CHECKPOINT*/

    //check for valid input
    if (argc != 12) {
        std::cerr << "ERROR: Wrong usage!" << std::endl;
        return -1;
    }

    /*INITIALIZATION*/

    //read in variables
    std::ifstream infile_stream(argv[1]);
    std::string machine(argv[2]);
          double l_phero_bound = std::stod(argv[3]),//dynamic bounds
                 u_phero_bound = std::stod(argv[4]);

    const double alpha         = std::stod(argv[5]),
                 beta          = std::stod(argv[6]),
                 r             = std::stod(argv[7]),
                 q             = std::stod(argv[8]);

    const int numAnts    = std::stoul(argv[9]),
              iterations = std::stoul(argv[10]),
              n          = std::stoul(argv[11]);

    //initialize distances
    Acoa::Distance_Map distances;
    const Acoa::dmatrix_t points = distances.init(infile_stream);

    //initialize pheromone map
    const Acoa::idx_t N = distances.getDim();
    //MINMAX: Set upper bound arbitrarily high
    u_phero_bound = std::numeric_limits<unsigned>::max();

    //MINMAX: Set p_best from the beginning (it might be good make this an input argument,
    //however from the paper 0.05 is clearly the best choice)
    Acoa::scalar_t p_best = 0.05;
    Acoa::scalar_t p_dec = std::pow(p_best, 1.0/(N-1));
    Acoa::scalar_t avg = N/2.0;


    Acoa::Pheromone_Map pheromap(l_phero_bound, u_phero_bound, N, q, alpha, beta, r, distances);

    //initialize ants
    Acoa::Ant::setDistances(distances);
    std::vector<Acoa::Ant> ants(numAnts);

    //initialize Threads
    std::vector<std::thread> threadVec;
    threadVec.reserve(n-1);

    //define lambda for threads
    auto lambda = [] (std::vector<Acoa::Ant> const& ants, int j, int k) {
        std::vector<Acoa::Ant>& ants2 = const_cast<std::vector<Acoa::Ant>&>(ants);
        for (int i = j; i < k; ++i) {
            ants2[i].doRoundTrip();
        }
    };

    //initialize evaluation metrics
    unsigned shortest_sol = std::numeric_limits<unsigned>::max();
    std::vector<Acoa::idx_t> shortest_route(N);
    std::pair<std::vector<Acoa::idx_t>, Acoa::int_t> result;
    Acoa::Matrix_t updates(N);
    //Acoa::idx_t start = 0; //DEBUG

    /*TIME CHECKPOINT*/
    init.stop();
    main_loop.start();
    /*TIME CHECKPOINT*/

    /*MAIN LOOP*/
    unsigned iter=0;
    for ( ; iter<iterations; ++iter) {
        //std::cout<<"-----Iteration "<<i<<"-----"<<std::endl; //DEBUG

        /*TIME CHECKPOINT*/
        send.start();
        /*TIME CHECKPOINT*/

        //update transition probabilities
        Acoa::Matrix_t probs = pheromap.Transition_Probs();
        for (auto& ant : ants) {
            ant.sendProbabilities(probs);
        }


        /*TIME CHECKPOINT*/
        send.stop();
        send_total += send.duration();
        bound_update.start();
        /*TIME CHECKPOINT*/

        /*BOUND UPDATE*/
        //MINMAX : Update boundaries according to the formulas in the paper
        u_phero_bound = (1.0/r)*(1.0/shortest_sol);
        l_phero_bound = u_phero_bound*(1.0-p_dec)/(avg*p_dec);

        //prevent weird bounds
        if(l_phero_bound > u_phero_bound) {
            l_phero_bound = u_phero_bound;
        }

        //update bounds in the pheromone map
        pheromap.set_lower_bound(l_phero_bound);
        pheromap.set_upper_bound(u_phero_bound);
        /*END BOUND UPDATE*/

        /*TIME CHECKPOINT*/
        bound_update.stop();
        bound_update_total += send.duration();
        ant_run.start();
        /*TIME CHECKPOINT*/

        //let the ants run

        //std::cout << "start threading" << std::endl; //DEBUG

        /*TIME CHECKPOINT*/
        thread_init.start();
        /*TIME CHECKPOINT*/

        threadVec.clear();
        for (int i = 1; i < n; ++i) {
            threadVec.push_back(std::thread(lambda, std::ref(ants), (numAnts*i)/n, (numAnts*(i+1))/n));
        }

        /*TIME CHECKPOINT*/
        thread_init.stop();
        thread_init_total += thread_init.duration();
        single_thread.start();
        /*TIME CHECKPOINT*/

        lambda(std::ref(ants), 0, numAnts/n);

        /*TIME CHECKPOINT*/
        single_thread.stop();
        single_thread_total += single_thread.duration();
        thread_join.start();
        /*TIME CHECKPOINT*/

        for (auto& thread : threadVec) {
            thread.join();
        }

        /*TIME CHECKPOINT*/
        thread_join.stop();
        thread_join_total += thread_join.duration();
        /*TIME CHECKPOINT*/

        //std::cout << "Iteration " << iter << " done" << std::endl; //DEBUG

        /*TIME CHECKPOINT*/
        ant_run.stop();
        ant_run_total += ant_run.duration();
        receive.start();
        /*TIME CHECKPOINT*/

        //find best path of iteration
//        unsigned int sum_dist=0; //DEBUG
        result = ants[0].getPath();
        for (auto& ant : ants) {
//            sum_dist+=ant.getPath().second; //DEBUG
            if (ant.getPath().second < result.second){
                result = ant.getPath();
            }
        }

//        std::cout<<"IT: "<<i<<std::endl; //DEBUG
//        std::cout<<"Shortest: "<<result.second<<std::endl; //DEBUG
//        std::cout<<"Avg Dist: "<<sum_dist/numAnts<<std::endl; //DEBUG


        /*TIME CHECKPOINT*/
        receive.stop();
        receive_total += receive.duration();
        update.start();
        /*TIME CHECKPOINT*/

        Acoa::scalar_t update_val = q/result.second;
        Acoa::idx_t lastnode = result.first[0];
        updates(result.first[N-1], lastnode) += update_val;
        for (Acoa::idx_t i = 1; i < N; ++i) {
            updates(lastnode, result.first[i]) += update_val;
            lastnode = result.first[i];
        }

        //Update the pheromone levels on the paths
        pheromap.Pheromone_Decrease();
        pheromap.Pheromone_Update(updates);


        //Update best performance
        if (result.second < shortest_sol) {
            shortest_route = result.first;
            shortest_sol = result.second;
        }

        /*TIME CHECKPOINT*/
        update.stop();
        update_total += update.duration();
        termination.start();
        /*TIME CHECKPOINT*/

        //Termination by Avg Branching Factor
        auto avg_bf = pheromap.Avg_Branching_Factor(l_phero_bound, u_phero_bound, 0.1);
        //std::cout<<"New Avg Branching Factor: "<<avg_bf<<std::endl; //DEBUG
        if (avg_bf == 2 && iter>10){
            std::cout<<"Termination in Iteration "<<iter<<" by Avg-Branching-Factor."<<std::endl;
            break;
        }

        /*TIME CHECKPOINT*/
        termination.stop();
        termination_total += termination.duration();
        /*TIME CHECKPOINT*/
    }

    /*TIME CHECKPOINT*/
    main_loop.stop();
    /*TIME CHECKPOINT*/

    std::cout<<"-----FINAL-----"<<std::endl;
    std::cout << "Length of shortest round-trip: " << shortest_sol << std::endl;
    std::cout << "Path of shortest round-trip: ";
    for (Acoa::idx_t& i : shortest_route) std::cout << i << " ";
    std::cout << std::endl;

    /*TIME CHECKPOINT*/
    total.stop();
    /*TIME CHECKPOINT*/

    /*PRINT TIMER INFO*/
    std::string file(argv[1]);

    std::string time_file = file.substr(0, file.size()-2) + "time";
    std::ofstream t_out(time_file, std::ofstream::out); //rewrite file
    t_out << "---- " << time_file << " ----\nAll times reported in seconds as total time spent in subroutine" << std::endl;
//    t_out << "MACHINE: " << machine << std::endl;
    t_out << "NUMANTS = " << numAnts << std::endl;
    t_out << "ITERATIONS = " << iter << std::endl;
    t_out << "THREADS/PROCESSES = " << n << std::endl;
    t_out << "---- Total Time ----\n" << total.duration() << std::endl;
    t_out << "---- Initialization Time ----\n" << init.duration() << std::endl;
    t_out << "---- Time spent in Main Loop ----\n" << main_loop.duration() <<std::endl;
    t_out << "---- Ant Run Time ----\n" << ant_run_total << std::endl;
    t_out << "---- Probs Send Time ----\n" << send_total << std::endl;
    t_out << "---- Path Receive Time ----\n" << receive_total << std::endl;
    t_out << "---- Bound Update Time ----\n" << receive_total << std::endl;
    t_out << "---- Time spent updating Probs ----\n" << update_total << std::endl;
    t_out << "---- Time spent checking for termination ----\n" << termination_total << std::endl;

    /*PRINT ALL INFO TO CSV*/
    std::string csv_file = file.substr(0, file.size()-2) + "csv";
    std::ifstream csv_in(csv_file);
    std::ofstream csv_out(csv_file, std::ofstream::out | std::ofstream::app); //append to file
    if (csv_in.peek() == std::ifstream::traits_type::eof()) {
        csv_out << "NUMANTS,ITERATIONS,THREADS/PROCESSES,time_tot,"
                << "time_initialization,time_main_loop,time_ant_run,"
                << "time_probs_send,time_path_receive,time_updating_probs,"
                << "path_length,alpha,beta,r,q,termination,thread_init,"
                << "thread_join,single_thread"
                << std::endl;
    }
    csv_out << std::setprecision(14)
            << machine              << ","
            << numAnts              << ","
            << iter                 << ","
            << n                    << ","
            << total.duration()     << ","
            << init.duration()      << ","
            << main_loop.duration() << ","
            << ant_run_total        << ","
            << send_total           << ","
            << receive_total        << ","
            << update_total         << ","
            << shortest_sol         << ","
            << alpha                << ","
            << beta                 << ","
            << r                    << ","
            << q                    << ","
            << termination_total    << ","
            << thread_init_total    << ","
            << thread_join_total    << ","
            << single_thread_total  << std::endl;

    /*PRINT PATH*/
    std::string path_file = file.substr(0, file.size()-2) + "path";
    std::cout << path_file << std::endl; //DEBUG
    std::ofstream path_out(path_file, std::ofstream::out); //rewrite file
    path_out << "x_vals,y_vals" << std::endl;
    //std::cout << "test"; //DEBUG
    for (Acoa::idx_t idx = 0; idx < N; ++idx) { //shortest path must have length N
        path_out << points[shortest_route[idx] * 2] << "," << points[shortest_route[idx] * 2 + 1] << std::endl;
    }
    path_out <<  points[shortest_route[0] * 2] << "," << points[shortest_route[0] * 2 + 1] << std::endl;

}
