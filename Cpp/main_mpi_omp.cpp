/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <limits>
#include <algorithm>
#include <iomanip>
#include <mpi.h>
#include <omp.h>


#include "src/ant.hpp"
#include "timer/timer.hpp"

int main(int argc, char** argv) {
    int rank,size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

//    #pragma omp parallel for
//    for (int i=0; i<8; ++i){
//        std::cout<<"rank "<<rank<<" with "<<omp_get_thread_num()<<std::endl;
//    }

    double ant_run_total = 0, update_total = 0, send_total = 0, receive_total = 0, bound_update_total = 0, termination_total = 0;

    Timer init, main_loop, ant_run, bound_update, update, send, receive, total, termination;

    if(rank==0) {
        /*TIME CHECKPOINT*/
        total.start();
        init.start();
        /*TIME CHECKPOINT*/

        //check for valid input
        if (argc != 13) {
            std::cerr << "ERROR: Wrong usage!" << std::endl;
            return -1;
        }
    }

    /*INITIALIZATION*/

    //read in variables
    std::ifstream infile_stream(argv[1]);
    std::string machine(argv[2]);
    double l_phero_bound = std::stod(argv[3]),//dynamic bounds
            u_phero_bound = std::stod(argv[4]);

    const double alpha = std::stod(argv[5]),
            beta = std::stod(argv[6]),
            r = std::stod(argv[7]),
            q = std::stod(argv[8]);

    const int numAnts = std::stoul(argv[9]),
            iterations = std::stoul(argv[10]),
            processes = std::stoul(argv[11]);

    //initialize distances
    Acoa::Distance_Map distances;
    const Acoa::dmatrix_t points = distances.init(infile_stream);
    const Acoa::idx_t N = distances.getDim();

    //DEFINE TYPE
    MPI_Datatype vector_dt_d;
    MPI_Type_vector(1, N * N, 0, MPI_DOUBLE, &vector_dt_d);
    MPI_Type_commit(&vector_dt_d);

    MPI_Datatype vector_dt_i;
    MPI_Type_vector(1, N, 0, MPI_INT, &vector_dt_i);
    MPI_Type_commit(&vector_dt_i);


    //initialize pheromone map
    //MINMAX: Set upper bound arbitrarily high
    u_phero_bound = std::numeric_limits<unsigned>::max();

    //p set to 0.05 given by refrence paper
    Acoa::scalar_t p_best = 0.05;
    Acoa::scalar_t p_dec = std::pow(p_best, 1.0 / (N - 1));
    Acoa::scalar_t avg = N / 2.0;

    Acoa::Pheromone_Map pheromap(l_phero_bound, u_phero_bound, N, q, alpha, beta, r, distances);

    //initialize evaluation metrics
    int shortest_sol = std::numeric_limits<int>::max();
    std::vector <Acoa::idx_t> shortest_route(N);
    std::pair <std::vector<Acoa::idx_t>, int> result;
    result.first = shortest_route;
    Acoa::Matrix_t updates(N);

    int work = numAnts/size;

    if(rank < numAnts%size){
        work++;
    }



    //initialize ant
    Acoa::Ant::setDistances(distances);
    std::vector<Acoa::Ant> ants(work);
    std::pair <std::vector<Acoa::idx_t>, unsigned> iter_best;
    iter_best.first = shortest_route;

    if(rank==0){
        /*MAIN LOOP*/
        /*TIME CHECKPOINT*/
        init.stop();
        main_loop.start();
        /*TIME CHECKPOINT*/
    }
    int iter = 0;
    Acoa::Matrix_t probs(N);

    for (; iter < iterations; ++iter) {

        if(rank==0){
            /*TIME CHECKPOINT*/
            send.start();
            /*TIME CHECKPOINT*/
        }

        //update transition probabilities
        probs = pheromap.Transition_Probs();
        std::vector<double> probs_vec(N * N);
        probs_vec=probs.get_vector();

        #pragma omp parallel for
        for (int i=0; i<work; ++i) ants[i].sendProbabilities(probs);

        if(rank==0){
            /*TIME CHECKPOINT*/
            send.stop();
            send_total += send.duration();
            bound_update.start();
            /*TIME CHECKPOINT*/
        }

        /*BOUND UPDATE*/
        //MINMAX : Update boundaries according to the formulas in the paper
        u_phero_bound = (1.0 / r) * (1.0 / shortest_sol);
        l_phero_bound = u_phero_bound * (1.0 - p_dec) / (avg * p_dec);

        //prevent weird bounds (should never occur)
        if (l_phero_bound > u_phero_bound) {
            l_phero_bound = u_phero_bound;
        }

        //update bounds in the pheromone map
        pheromap.set_lower_bound(l_phero_bound);
        pheromap.set_upper_bound(u_phero_bound);
        /*END BOUND UPDATE*/

        if(rank==0){
            /*TIME CHECKPOINT*/
            bound_update.stop();
            bound_update_total += send.duration();
            ant_run.start();
            /*TIME CHECKPOINT*/
        }

        //let the ants run
        iter_best.second = std::numeric_limits<int>::max();

        #pragma omp parallel for
        for(int i=0; i<work; ++i) {
            ants[i].doRoundTrip();
        }
        for (auto& ant: ants){
            if (ant.getPath().second < iter_best.second){
                iter_best.first = ant.getPath().first;
                iter_best.second = ant.getPath().second;
            }
        }

        if(rank==0){
            /*TIME CHECKPOINT*/
            ant_run.stop();
            ant_run_total += ant_run.duration();
            receive.start();
            /*TIME CHECKPOINT*/
        }

        //find best path of iteration
        int min_lenght = std::numeric_limits<int>::max();
        int globalres[2];
        globalres[1]= rank;
        int temp[2];
        temp[0] = iter_best.second;
        temp[1] = rank;
        MPI_Allreduce(temp, globalres, 1, MPI_2INT, MPI_MINLOC, MPI_COMM_WORLD);

        MPI_Bcast(&iter_best.first[0], 1, vector_dt_i, globalres[1], MPI_COMM_WORLD);
        result.first=iter_best.first;
        result.second = globalres[0];

        if(rank == 0){
            /*TIME CHECKPOINT*/
            receive.stop();
            receive_total += receive.duration();
            update.start();
            /*TIME CHECKPOINT*/
        }

        Acoa::scalar_t update_val = q / result.second;
        Acoa::idx_t lastnode = result.first[0];
        updates(result.first[N - 1], lastnode) += update_val;
        for (Acoa::idx_t i = 1; i < N; ++i) {
            updates(lastnode, result.first[i]) += update_val;
            lastnode = result.first[i];
        }
        //Update the pheromone levels on the paths
        pheromap.Pheromone_Decrease();
        pheromap.Pheromone_Update(updates);
        //Update best performance
        if (result.second < shortest_sol) {
            shortest_route = result.first;
            shortest_sol = result.second;
        }

        if(rank==0){
            /*TIME CHECKPOINT*/
            update.stop();
            update_total += update.duration();
            termination.start();
            /*TIME CHECKPOINT*/
        }

        double avg_bf;
        avg_bf = pheromap.Avg_Branching_Factor(l_phero_bound, u_phero_bound, 0.1);
        //MPI_Bcast(&avg_bf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        if (avg_bf == 2 && iter > 10) {
            std::cout <<"rank "<<rank<<": Termination in Iteration " << iter << " by Avg-Branching-Factor." << std::endl;
            break;
        }

        if(rank==0){
            /*TIME CHECKPOINT*/
            termination.stop();
            termination_total += termination.duration();
            /*TIME CHECKPOINT*/
        }

        if(rank==0){
            /*TIME CHECKPOINT*/
            main_loop.stop();
            /*TIME CHECKPOINT*/
        }
    }//end for loop

    if(rank==0){


        std::cout << "Length of shortest round-trip: " << shortest_sol << std::endl;
        std::cout << "Path of shortest round-trip: ";
        for (Acoa::idx_t &i : shortest_route) std::cout << i << " ";
        std::cout << std::endl;

        /*TIME CHECKPOINT*/
        total.stop();
        /*TIME CHECKPOINT*/

        /*PRINT TIMER INFO*/
        std::string file(argv[1]);

        std::string time_file = file.substr(0, file.size() - 2) + "time";
        std::ofstream t_out(time_file, std::ofstream::out); //rewrite file
        t_out << "---- " << time_file << " ----\nAll times reported in seconds as total time spent in subroutine"
              << std::endl;
        //    t_out << "MACHINE: " << machine << std::endl;
        t_out << "NUMANTS = " << numAnts << std::endl;
        t_out << "ITERATIONS = " << iter << std::endl;
        t_out << "THREADS/PROCESSES = " << processes << std::endl;
        t_out << "---- Total Time ----\n" << total.duration() << std::endl;
        t_out << "---- Initialization Time ----\n" << init.duration() << std::endl;
        t_out << "---- Time spent in Main Loop ----\n" << main_loop.duration() << std::endl;
        t_out << "---- Ant Run Time ----\n" << ant_run_total << std::endl;
        t_out << "---- Probs Send Time ----\n" << send_total << std::endl;
        t_out << "---- Path Receive Time ----\n" << receive_total << std::endl;
        t_out << "---- Bound Update Time ----\n" << receive_total << std::endl;
        t_out << "---- Time spent updating Probs ----\n" << update_total << std::endl;
        t_out << "---- Time spent checking for termination ----\n" << termination_total << std::endl;

        /*PRINT ALL INFO TO CSV*/
        std::string csv_file = file.substr(0, file.size() - 2) + "csv";
        std::ifstream csv_in(csv_file);
        std::ofstream csv_out(csv_file, std::ofstream::out | std::ofstream::app); //append to file
        if (csv_in.peek() == std::ifstream::traits_type::eof()) {
            csv_out << "NUMANTS,ITERATIONS,THREADS/PROCESSES,time_tot,"
                    << "time_initialization,time_main_loop,time_ant_run,"
                    << "time_probs_send,time_path_receive,time_updating_probs,"
                    << "path_length,alpha,beta,r,q,termination"
                    << std::endl;
        }
        csv_out << std::setprecision(14)
                << machine << ","
                << numAnts << ","
                << iter << ","
                << processes << ","
                << total.duration() << ","
                << init.duration() << ","
                << main_loop.duration() << ","
                << ant_run_total << ","
                << send_total << ","
                << receive_total << ","
                << update_total << ","
                << shortest_sol << ","
                << alpha << ","
                << beta << ","
                << r << ","
                << q << ","
                << termination_total << std::endl;

        /*PRINT PATH*/
        std::string path_file = file.substr(0, file.size() - 2) + "path";
        std::ofstream path_out(path_file, std::ofstream::out); //rewrite file
        path_out << "x_vals,y_vals" << std::endl;
        for (Acoa::idx_t idx = 0; idx < N; ++idx) { //shortest path must have length N
            path_out << points[shortest_route[idx] * 2] << "," << points[shortest_route[idx] * 2 + 1] << std::endl;
        }
        path_out <<  points[shortest_route[0] * 2] << "," << points[shortest_route[0] * 2 + 1] << std::endl;
    }
    MPI_Finalize();
    return 0;
}


