/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#ifndef DISTANCEMAP_HPP
#define DISTANCEMAP_HPP

#include <vector>
#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <cmath>
#include <array>



namespace Acoa {

    using idx_t = int;
    using int_t = unsigned;
    using dmatrix_t = std::vector<int_t>;

    //bool für ganze Matrix
    //using nmatrix_t = std::vector<bool>;
    //idx_t für Vektor mit 30 Elementen
    using nmatrix_t = std::vector<idx_t>;

    class Distance_Map {

    public:
        Distance_Map() {};
        const dmatrix_t init(std::ifstream& file);
        int_t const& operator() (idx_t const& a, idx_t const& b) const;
        idx_t getDim() const; //return number of rows/columns
        nmatrix_t get_neighbour_matrix();
    private:

        idx_t dimension_;
        dmatrix_t distance_matrix;
        nmatrix_t neighbour_matrix; //n*n bool matrix with (i,j)=1 if j one of closest 30 to i

        void setup_distances(dmatrix_t& points, dmatrix_t& distance_matrix, int size);
        void setup_neighbour_matrix(nmatrix_t& close_nodes ,int size);
        int_t rs_dist(std::vector<int_t> a, std::vector<int_t> b);
    };
}

#endif // DISTANCEMAP_HPP
