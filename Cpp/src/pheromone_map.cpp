/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

 #include "pheromone_map.hpp"

 namespace Acoa {
    //Initialize Matrix with 0 by default
     Matrix_t::Matrix_t(idx_t const& dim):phero_matrix(dim*dim, 0.0), dimension(dim){}

     void Matrix_t::update_vector(std::vector<double> a){
         phero_matrix=a;
     }

     std::vector<double> Matrix_t::get_vector(){
         return phero_matrix;
     }


     scalar_t& Matrix_t::operator() (idx_t const& a, idx_t const& b){
         assert(a < dimension);
         assert(b < dimension);
         if (a < b)
             return phero_matrix[dimension*a+b];
         else
             return phero_matrix[dimension*b+a];
     }

     scalar_t const& Matrix_t::operator() (idx_t const& a, idx_t const& b) const {
         assert(a < dimension);
         assert(b < dimension);
         if (a < b)
             return phero_matrix[dimension*a+b];
         else
             return phero_matrix[dimension*b+a];
     }

     Matrix_t& Matrix_t::operator=(const Matrix_t& other){
       assert(dimension == other.dimension);
       phero_matrix = other.phero_matrix;
       return *this;
     }


      //Constructor für Pheromone_Map
      Pheromone_Map::Pheromone_Map(scalar_t lowerbound_input, scalar_t upperbound_input, int_t nodecount_input, int_t q_constant_input,scalar_t alpha_constant_input, scalar_t beta_constant_input, scalar_t r, Distance_Map& distance_map_input):
      lowerbound(lowerbound_input), upperbound(upperbound_input), nodecount(nodecount_input),
       q_constant(q_constant_input),alpha_constant(alpha_constant_input), beta_constant(beta_constant_input),
       distances(distance_map_input), r_constant(r), pheromone_levels(nodecount_input), transition_probs(nodecount_input){
         for(idx_t i = 0; i< nodecount; i++){
           for (idx_t j = i; j < nodecount; j++) {
             pheromone_levels(i,j) = upperbound; //initialize with upperbound for minmax 
           }
         }
       }


      bool Pheromone_Map::Pheromone_Decrease(){
        int n = nodecount; //brauchen dimension der Matrix
        for(idx_t i=0; i<n; i++){
            for(idx_t j=i; j<n; j++){
                scalar_t tmp = pheromone_levels(i,j) * (1-r_constant);

                if(tmp < lowerbound) {//respect the boundaries
                pheromone_levels(i,j) = lowerbound;
                }
                else {
                  pheromone_levels(i,j) = tmp;
                }
            }
        }
        return true;

      }

      bool Pheromone_Map::Pheromone_Update(Matrix_t& pheromone_updates){
        int n = nodecount; //brauchen dimension der Matrix
        for(idx_t i = 0; i < n; i++){
          for (idx_t j = i; j < n; j++) {
            //only write if the edge belongs to the update path (efficiency)
            if(pheromone_levels(i,j)>0){

              scalar_t tmp = pheromone_levels(i,j) + pheromone_updates(i,j);
              pheromone_updates(i,j) = 0; //PREVENT CUMULATION OF UPDATES

              if(tmp > upperbound) {
                pheromone_levels(i,j) = upperbound;
                } //respect bounds
              else {
                pheromone_levels(i,j) = tmp;
              }
            }
          }
        }
        return true;
      }

      void Pheromone_Map::set_lower_bound(scalar_t lb){
        lowerbound = lb;
      }

      void Pheromone_Map::set_upper_bound(scalar_t ub){
        upperbound = ub;
      }

      Matrix_t Pheromone_Map::Transition_Probs(){
        int n = nodecount; //brauchen dimension der Matrix
        for(idx_t i = 0; i < n; i++){
          for (idx_t j = i; j < n; j++) {
            transition_probs(i,j) = std::pow(pheromone_levels(i,j), alpha_constant) * std::pow(1./distances(i,j), beta_constant);
          }
          transition_probs(i,i) = 0;
        }
        return transition_probs;
      }

     scalar_t Pheromone_Map::Avg_Branching_Factor(scalar_t l_phero_bound, scalar_t u_phero_bound, scalar_t lambda){ //change to refrences! Is this "efficient"?
         scalar_t avg_bf = 0.;
         int n = nodecount;

         for(idx_t i = 0; i < n; i++){

             scalar_t min_phero_i = u_phero_bound;
             scalar_t max_phero_i = l_phero_bound;
             for (idx_t j = 0; j < n; j++) {
                 if(pheromone_levels(i,j) > max_phero_i){
                     max_phero_i = pheromone_levels(i,j);
                 }
                 if(pheromone_levels(i,j) < min_phero_i){
                     min_phero_i = pheromone_levels(i,j);
                 }
             }
             scalar_t threshold_i = min_phero_i + lambda*(max_phero_i-min_phero_i);

             idx_t bf_i=0;
             for (idx_t j = 0; j < n; j++) {
                 if(pheromone_levels(i,j) > threshold_i){
                     bf_i +=1;
                 }
             }

             avg_bf+= bf_i;
         }
         avg_bf/=n;

         return avg_bf;
     }

 }
