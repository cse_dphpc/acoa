/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#ifndef PHEROMONEMAP_HPP
#define PHEROMONEMAP_HPP
#include <vector>
#include <cmath>

#include "distance_map.hpp"

namespace Acoa {
    using int_t = unsigned;
    using scalar_t = float;
    using indx_t = int;

    class Matrix_t {
    public:
        Matrix_t(idx_t const& dim);
        scalar_t& operator() (idx_t const& a, idx_t const& b);
        scalar_t const& operator() (idx_t const& a, idx_t const& b) const;
        Matrix_t& operator= (const Matrix_t& other);
    private:
        using pmatrix_t = std::vector<scalar_t>;
        idx_t dimension;
        pmatrix_t phero_matrix;
    };


    class Pheromone_Map {
    public:
        Pheromone_Map(scalar_t lowerbound, scalar_t upperbound, int_t nodecount, int_t q_constant, scalar_t alpha_constant, scalar_t beta_constant, scalar_t r_constant, Distance_Map& distances);
        bool Pheromone_Decrease();
        //bool Pheromone_Increase(bitedgemap<visited> , roundtrip_length);
        bool Pheromone_Update(Matrix_t& pheromone_updates);
        Matrix_t Transition_Probs();
        scalar_t Avg_Branching_Factor(scalar_t l_phero_bound, scalar_t u_phero_bound, scalar_t lambda);

        //MINMAX: helper functions
        void set_upper_bound(scalar_t );
        void set_lower_bound(scalar_t );

    private:
        Distance_Map& distances;
        Matrix_t pheromone_levels;
        Matrix_t transition_probs;
        const scalar_t alpha_constant;
        const scalar_t beta_constant;
        scalar_t lowerbound; //MINMAX => not const bounds
        scalar_t upperbound;
        const int_t nodecount;
        const int_t q_constant;
        const scalar_t r_constant; //rate of decrease, woher wollen wir die nehmen? input oder einfach festlegen?
    };
}

#endif // MAP_HPP
