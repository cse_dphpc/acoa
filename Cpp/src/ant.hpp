/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */



#ifndef ANTS_HPP
#define ANTS_HPP

#include <vector>
#include <cmath>
#include <random>
#include <ctime>
#include <cstdlib>
#include <thread>

#include "pheromone_map.hpp"

namespace Acoa {

    using idx_t = int; // depending on the input size, the index type can be changed
    using scalar_t = float; //usually float or double

    /**
     * Each instance of the Ant class simulates one Ant of the colony. The main
     * tasks include doing Hamiltonian tours and communicating with the driver.
     */
    class Ant {
    public:

        /**
         * Ant Class constructor. In this constructor, all private variables
         * are initialized and the RNG is set up.
         */
        Ant();

        /**
         * Set the Distance Matrix for all ants (static function)
         * @param distances The new Distance Matrix
         * @return True if operation succesful, false if not
         */
        static bool setDistances(Distance_Map& distances);

        /**
         * Send new transition probabilities from driver to a single ant
         * @param transition_probs The new transition probability Matrix
         * @return True if operation succesful, false if not
         */
        bool sendProbabilities(Matrix_t const& transition_probs);

        /**
         * Lets the ant do a single Hamiltonian graph traversion
         * @param start The index of the node from which the tour should be started
         * @return True if operation succesful, false if not
         */
        bool doRoundTrip(idx_t const& start);

        /**
         * Lets the ant do a single Hamiltonian tour, starting from a random vertex
         * @return True if operation succesful, false if not
         */
        bool doRoundTrip();

        /**
         * Returns the path and length of the last Hamiltonian tour performed
         * @return A Pair, containing path in first and length in second argument
         */
        std::pair<std::vector<idx_t>, int_t> getPath(); // <path, distance_covered>

        /**
         * Lets the ant return a thread in which one Hamiltonian Tour is done.
         * Not used in any final version due to large Threading overhead
         * @return A Thread containing the workload of doing a single Hamiltonian Tour
         */
        std::thread threadedRoundTrip();

    private:
        // static/shared variables
        static Distance_Map distances; // stores distances between each city

        //local variables
        int_t path_length; // stores length of current Hamiltonian tour
        std::random_device rd; // random device
        std::mt19937 gen; // generator
        std::vector<idx_t> used_path; // (start, end], where end = start
        std::vector<bool> visited_nodes; // keeps track of cities that were already visited
        Matrix_t transition_probs; // stores transition probabilities for each edge
    };
}

#endif // ANTS_HPP
