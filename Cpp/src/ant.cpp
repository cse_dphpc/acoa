/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#include "ant.hpp"

namespace Acoa {

    Distance_Map Ant::distances = Distance_Map();
    //Matrix_t m(0);
    //Matrix_t Ant::transition_probs = m;

    /**
     * Ant Class constructor. In this constructor, all private variables
     * are initialized and the RNG is set up.
     */
    Ant::Ant():
    used_path(std::vector<idx_t>(distances.getDim())), visited_nodes(std::vector<bool> (distances.getDim()))
    , path_length(0), transition_probs(distances.getDim())
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        srand(23);
    }

    /**
     * Send new transition probabilities from driver to a single ant
     * @param transition_probs The new transition probability Matrix
     * @return True if operation succesful, false if not
     */
    bool Ant::sendProbabilities(Matrix_t const& transition_probs_input){
      transition_probs = transition_probs_input;
      return true;
    }

    /**
     * Set the Distance Matrix for all ants (static function)
     * @param distances The new Distance Matrix
     * @return True if operation succesful, false if not
     */
    bool Ant::setDistances(Distance_Map& distances_in) {
        distances = distances_in;
        return true;
    }

    /**
     * Lets the ant do a single Hamiltonian tour, starting from a random vertex
     * @return True if operation succesful, false if not
     */
    bool Ant::doRoundTrip() {
        idx_t n = distances.getDim();
        doRoundTrip(rand()%n);
    }

    /**
     * Lets the ant do a single Hamiltonian graph traversion
     * @param start The index of the node from which the tour should be started
     * @return True if operation succesful, false if not
     */
    bool Ant::doRoundTrip(idx_t const& start){
      path_length = 0;
      auto close_neighbours = distances.get_neighbour_matrix();
      std::fill(visited_nodes.begin(), visited_nodes.end(), 0);
      int_t n = distances.getDim(); //brauchen dimension der Matrix, sprich Anzahl Orte
      int_t num_nodes_to_go = n-1; //Anzahl Orte die noch zu besuchen sind
      int_t iteration_num = 0; //Anzahl der schon besuchten Orte
      idx_t current_position = start;
      idx_t next_position;
      visited_nodes[start] = 1;
      //scalar_t normalizer;
      int remaining_close;
      std::vector<scalar_t> probabilities(n);
      idx_t potential_next_node;


      while(num_nodes_to_go > 0){

        remaining_close =0;
        std::fill(probabilities.begin(), probabilities.end(),0);
        for(int i = 0; i < 30; i++){
          potential_next_node = close_neighbours[i];
          if(visited_nodes[potential_next_node]==0){
            remaining_close++;
            probabilities[potential_next_node] = transition_probs(current_position, potential_next_node);
          }
        }
        //hier können wir einstellen wie lange wir nur auf die nodes schauen wollen die in der Nähe sind
        //würde intuitiv etwas größeres als 0 nehmen
        if(remaining_close>1){
          //std::random_device rd;
          //std::mt19937 gen(rd());
          std::discrete_distribution<> distribution(probabilities.begin(), probabilities.end());
          next_position = distribution(gen);

          assert(next_position < n);
          visited_nodes[next_position] = true;
          assert(visited_nodes[next_position] == true);
          assert(iteration_num < n);
          used_path[iteration_num] = next_position;
          path_length += distances(current_position, next_position);
          current_position = next_position;
          assert(visited_nodes[current_position] == true);

          num_nodes_to_go--;
          iteration_num++;

        }
        else{


          for(idx_t i = 0; i < n; i++){
            if(visited_nodes[i] == false){
              probabilities[i] = transition_probs(current_position, i);// normalizer;
            }
            else probabilities[i] = 0;
          }

          //std::default_random_engine generator;
          //std::random_device rd;
          //std::mt19937 gen(rd());
          std::discrete_distribution<> distribution(probabilities.begin(), probabilities.end());
          next_position = distribution(gen);

          assert(next_position < n);
          visited_nodes[next_position] = true; //ich glaube wir brauchen visited_nodes gar nicht
          assert(visited_nodes[next_position] == true);
          assert(iteration_num < n);
          used_path[iteration_num] = next_position;
          path_length += distances(current_position, next_position);
          current_position = next_position;
          assert(visited_nodes[current_position] == true);

          num_nodes_to_go--;
          iteration_num++;

        }
        //std::cout << "Run done" << std::endl; //DEBUG
      }
      assert(iteration_num < n);
      path_length += distances(current_position, start);
      used_path[iteration_num] = start;
      return true;
    }

    /**
     * Returns the path and length of the last Hamiltonian tour performed
     * @return A Pair, containing path in first and length in second argument
     */
    std::pair<std::vector<idx_t>, int_t> Ant::getPath() {
      return std::make_pair(used_path, path_length);
    }

    /**
     * Lets the ant return a thread in which one Hamiltonian Tour is done.
     * Not used in any final version due to large Threading overhead
     * @return A Thread containing the workload of doing a single Hamiltonian Tour
     */
    std::thread Ant::threadedRoundTrip() {
        //std::cout << "hello from thread"; //DEBUG
        return std::thread([&] { doRoundTrip(); });
    }





}
//main body
