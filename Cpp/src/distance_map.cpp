/**
 * Course Project: Ant Colony Optimisation
 * Design of Parallel and High Performance Computing, Fall 2019
 * Authors: Aurelio Dolfini, Dominik Helmreich, Marc Wanner, Jan Stratmann
 * Developed at ETH Zurich
 */

#include "distance_map.hpp"

namespace Acoa {


    int_t Distance_Map::rs_dist(std::vector<int_t> a, std::vector<int_t> b){
        assert(a.size() == 2);
        assert(b.size() == 2);
        int_t dist = (a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]);
        dist = std::round(std::sqrt(dist));
        return dist;
    }

    void Distance_Map::setup_distances(dmatrix_t& points, dmatrix_t& dist_matrix, int size){
        for(int i=0; i<size; i++){
            for(int j=0; j<i; j++){
                assert(j*2+1 < points.size());
                assert(i*2+1 < points.size());
                std::vector<int_t> p1 = {points[i*2], points[i*2+1]};
                std::vector<int_t> p2 = {points[j*2], points[j*2+1]};
                assert(j*size+i < dist_matrix.size());
                dist_matrix[j*size+i] = rs_dist(p1, p2);
            }
        }

    }


    //für vektor mit 30 Elementen
    void Distance_Map::setup_neighbour_matrix(nmatrix_t& close_nodes ,int size){
        assert(size>=30);
        //shortest_distances_matrix = bmatrix_t (size*size);
        std::vector<int_t> neighbours(size);
        for(int i=0; i<size; i++){
            for(int j=0; j<size; j++){
              if (i < j)
                  neighbours[j] = distance_matrix[i*size + j];
              else
                  neighbours[j] = distance_matrix[j*size + i];

            }

            idx_t max_pos = std::distance(neighbours.begin() ,std::max_element(neighbours.begin(), neighbours.end()) );
            int_t max_distance = neighbours[max_pos];
            for (int j = 0; j < 30; j++) {
              auto temp = std::distance(neighbours.begin(),std::min_element(neighbours.begin(), neighbours.end()) );
              close_nodes[j] = temp;
              neighbours[temp] = max_distance;
            }
        }

    }

    nmatrix_t Distance_Map::get_neighbour_matrix(){
      return neighbour_matrix;
    }

    const dmatrix_t Distance_Map::init(std::ifstream& file) {
        std::string line;
        std::string delimiter = " ";
        assert(file.is_open());

        for(int i=0; i<3; i++){//jump first 4 lines
            getline(file, line);
        }

        getline(file, line);//get number of points
        for(int j=0; j<3; j++) {
            int begin = 0;
            int end = line.find(delimiter);
            std::string token = line.substr(begin, end);
            auto it = token.begin();
            if(std::isdigit(*it) ){
                //std::cout<<token;
                dimension_ = atoi(token.c_str());
            }
            line.erase(begin, end+1);
        }

        assert(dimension_ > 0);

        dmatrix_t points(dimension_*2);
        distance_matrix.resize(dimension_*dimension_);


        for(int i=0; i<2; i++){
            getline(file, line);
        }

        for(int i = 0; getline(file, line) && i<dimension_; i++){

            for(int j=0; j<3; j++) {//jump all spaces (number varies)
                for(int k=0; line.substr(0,1)==delimiter;k++){
                line.erase(0,1);
                }

                int begin = 0;//read in point
                int end = line.find(delimiter);
                std::string token = line.substr(begin, end);
                if(j > 0){
                    points[i*2+j-1] = atof(token.c_str());
                }
                line.erase(begin, end+1);
            }
        }
        setup_distances(points, distance_matrix, dimension_);

        // erstellung von bool matrix
        //neighbour_matrix = std::vector<bool>(dimension_*dimension_);
        //std::fill(neighbour_matrix.begin(), neighbour_matrix.end(), 0);

        //erstelle Vector mit 30
        neighbour_matrix = std::vector<idx_t>(30);


        setup_neighbour_matrix(neighbour_matrix, dimension_);
        return points;
    }



    int_t const& Distance_Map::operator() (idx_t const& a, idx_t const& b) const{
        assert(a < dimension_);
        assert(b < dimension_);
        if (a < b)
            return distance_matrix[dimension_*a+b];
        else
            return distance_matrix[dimension_*b+a];
    }

    idx_t Distance_Map::getDim() const{
        return dimension_;
    }




}
