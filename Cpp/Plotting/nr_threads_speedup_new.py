from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

file = sys.argv[1]
number_of_ants = int(sys.argv[2])
img_type = sys.argv[3]
platforms=['Euler_MPI', 'Euler_GroupThread', 'Euler_OMP', 'Euler_Hybrid_2Ranks']
colors=['g', 'm', 'b', 'c']
signs=['v', 'o', '^', 'P']

data = pd.read_csv('Test_Cases/'+file+'.csv')
#*****1.measurements*****

#-----import data----- (no difference for numants & iterations)
EU_THREADS_all=[] #list of vectors
EU_time_tot_all=[] #list of vectors
NUMANTS_all=[] #list of vectors
for platform in platforms:
    EU_THREADS= data.filter(like=platform, axis=0)['THREADS/PROCESSES']
    EU_THREADS_all.append(EU_THREADS)
    NUMANTS = data.filter(like=platform, axis=0)['NUMANTS']
    NUMANTS_all.append(NUMANTS)
    EU_time_tot = data.filter(like=platform, axis=0)['time_tot']
    EU_time_tot_all.append(EU_time_tot)

#-----filter data for NUMANTS=number_of_ants-----
def filter_by_numants(data_vec, numants_vec, numants):
    filtered_data=[]
    for i,j in zip(data_vec, numants_vec):
        if j==numants:
            filtered_data.append(i)
    return filtered_data

for i in range(len(EU_THREADS_all)): #for each platform
    EU_THREADS_all[i] = np.asarray(filter_by_numants(EU_THREADS_all[i], NUMANTS_all[i], number_of_ants))
    EU_time_tot_all[i] = np.asarray(filter_by_numants(EU_time_tot_all[i], NUMANTS_all[i], number_of_ants))

#-----avg times-----
def filter_by_nrthreads(data_vec, threads_vec, nrthreads):
    filtered_data=[]
    for i,j in zip(data_vec, threads_vec):
        if j==nrthreads:
            filtered_data.append(i)
    return filtered_data

times_all=[]
threads_all=[]
for i in range(len(EU_THREADS_all)): #for each platform
    times=[]
    threads=[]
    max_threads = int(EU_THREADS_all[i].max())
    for thr in range(max_threads):
        filtered=np.asarray(filter_by_nrthreads(EU_time_tot_all[i], EU_THREADS_all[i], thr+1))
        if (filtered.size!=0):
            times.append(filtered.mean())
            threads.append(thr+1)
        # print(str(filtered.mean())+","+str(thr))
    times_all.append(np.asarray(times))
    threads_all.append(threads)


#-----times to speedup-----
def to_speedup(times, num_threads):
    speedups=[]
    t_1=0
    if num_threads[0]!=1:
        print("ERROR "+str(int(ants))+" ants: No Data For Single Thread")
        return
    else :
        t_1=times[0]
    for nt, t in zip(num_threads ,times):
        speedups.append(t_1/t)
        # opt_speedups.append(t_1/(t_1/nt))
    return speedups

speedups_all=[]
for i in range(len(threads_all)): #for each platform
    speedups = to_speedup(times_all[i], threads_all[i])
    speedups_all.append(speedups)


# -----PLOTTING-----
for i in range(len(threads_all)): #for each platform
    print("Plotting for platform="+str(platforms[i]))
    plt.plot(threads_all[i], speedups_all[i], marker=signs[i], color=colors[i], linewidth=3, label=platforms[i])


#*****2.optimal speedup***** anzupassen fuer Modell

#-----read data-----
EU_Master_time_seq = data.filter(like='Euler_Master', axis=0)['time_initialization']
EU_Master_time_prl = data.filter(like='Euler_Master', axis=0)['time_main_loop']
EU_Master_NUMANTS= data.filter(like='Euler_Master', axis=0)['NUMANTS']
EU_Master_time_seq = np.asarray(filter_by_numants(EU_Master_time_seq,EU_Master_NUMANTS, number_of_ants))
EU_Master_time_prl = np.asarray(filter_by_numants(EU_Master_time_prl,EU_Master_NUMANTS, number_of_ants))

#------avg data-----
time_seq = EU_Master_time_seq.mean()
time_prl = EU_Master_time_prl.mean()

#-----times to optimal speedup-----
def optimal_speedup(time_seq, time_prl, num_threads):
    speedups=[]
    for tr in num_threads:
        time = time_seq+time_prl/tr
        speedups.append((time_seq+time_prl)/time)
    return speedups

max_threads=[]
for i in threads_all:
    max_threads.append(np.asarray(i).max())
max_threads=np.asarray(max_threads).max()
opt_threads=np.linspace(1, max_threads, max_threads)
opt_speedups = optimal_speedup(time_seq, time_prl, opt_threads)

#-----plotting-----
print("Plotting Optimal SPUP")
plt.plot(opt_threads, opt_speedups, linestyle='--', marker='o', color='r', linewidth=3, label='Perfect Speedup')

#*****3.information*****
plt.plot([], [], linestyle='', marker='', color='w', linewidth=3, label=' ')
plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='Intel Xeon Gold 5118')
# plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='R=0.05, Q=10')
plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='Iterations=5000')


#-----set description-----
plt.title(file+" Speedup ("+str(number_of_ants)+" ants)" )
plt.xlabel("# Threads/Processes")
# plt.xticks(ticks=num_threads, labels=num_threads)
plt.ylabel("Speedup T_1 / T_p")
# plt.yscale('log')
plt.legend(loc='upper left')
plt.grid(True)
plt.tight_layout()

plt.savefig("Test_Cases/Plots_final/nr_threads__speedup_"+file+"_"+str(int(number_of_ants))+"ants_SIGNS."+img_type)
plt.close()