# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 17:59:33 2019

@author: Marc
"""

from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

"""
img_type = sys.argv[1]
num_threads = sys.argv[2]
num_iterations = sys.argv[3]
max_flopspcycle = sys.argv[4]
max_bandwidth = sys.argv[5]
num_ants = sys.argv[6]
time_parallel_ants_total = sys.argv[7]
num_cities = sys.argv[8]
time_seq_ants_total = sys.argv[9]
roundtrip_intensity = sys.argv[10] #tbd
"""

"""
Xeon_Gold: Max Bandwidth: 13.4 GB/s
           Max Flops/Cycle: ca. 19 GB/s per core
XeonE5-2680: Max Bandwidth: 8.5 GB/s
             Max Flops/Cycle: ca. 17 GB/s per core
"""

#---data is collected here---

img_type = "pdf"
num_threads = 6 #bitte anpassen
num_iterations = 5000 #bitte anpassen
max_flopspcycle = 19*(10**9)      #Intel Xeon Gold (one core)
max_bandwidth = 115*(10**9)        #Intel Xeon Gold
num_ants = 32  #bitte anpassen
time_parallel_ants_total = 6.28378 #bitte anpassen this corresponts to "Ant RUN TIME" or ant_run_total in main.cpp (in the parallel version)
num_cities = 127  #bitte anpassen
time_seq_ants_total = 33.6135 #bitte anpassen this corresponts to "Ant RUN TIME" or ant_run_total in main.cpp (in the sequential version)
roundtrip_flops_per_city = 1 #not sure wether I should take int instructions into account
roundtrip_rw_per_city = 8*(num_cities+np.log(num_cities))/num_cities #this is an approximation of all bytes that are read/written

#-----------------------------


max_parallel_flopspcycle = num_threads*max_flopspcycle

#intensity of all roundtrips
roundtrip_intensity = roundtrip_flops_per_city/roundtrip_rw_per_city

total_work = roundtrip_flops_per_city*num_cities**2*num_ants*num_iterations

#our performance depends on the intensity and the time it took
acoa_flopspcycle_seq = total_work/time_seq_ants_total
acoa_flopspcycle_parallel = total_work/time_parallel_ants_total

plt.title("Roofline Model Roundtrip")
plt.xlabel("I [Flops/byte]")
plt.ylabel("P [Flops/Cycle]")


x_steps=1000

x_roofl = np.linspace(0,10,x_steps)
y_roofl = max_bandwidth*x_roofl #roofline depending on the bandwith
#y_roofl2 = max_bandwidth*x_roofl/6

plt.loglog(x_roofl,y_roofl, color="blue", label="max. bandwidth="+str(max_bandwidth/10**9)+" GB/s")
#plt.loglog(x_roofl,y_roofl2, color="skyblue", label="max. bandwidth="+str(round(max_bandwidth/10**9/6))+" GB/s")
plt.loglog(x_roofl, max_flopspcycle*np.ones(x_steps), color="green", label="PI bond with one core="+str(round(max_flopspcycle/10**9))+" GFlops/s")
plt.loglog(x_roofl, max_parallel_flopspcycle*np.ones(x_steps), color="lightgreen", label="PI bond with "+str(num_threads)+" cores")
plt.loglog(roundtrip_intensity, acoa_flopspcycle_seq, color="red", marker="o", label="sequential acoa")
plt.loglog(roundtrip_intensity, acoa_flopspcycle_parallel, color="lightcoral", marker="o", label="acoa with "+str(num_threads)+" cores")
plt.legend(loc='best', prop={'size':10})
plt.tight_layout()
plt.grid(True)
#plt.show()
plt.savefig(r'rt_roofline.'+img_type)
#plt.close()





