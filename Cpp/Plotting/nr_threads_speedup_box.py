from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

file = sys.argv[1]
img_type = sys.argv[2]

data = pd.read_csv('Test_Cases/'+file+'.csv')


#-----import data----- (no difference for numants & iterations)
EU_THREADS = data.filter(like='Euler_MPI', axis=0)['THREADS/PROCESSES']
NUMANTS = data.filter(like='Euler_MPI', axis=0)['NUMANTS']
EU_time_tot = data.filter(like='Euler_MPI', axis=0)['time_tot']
# print(EU_THREADS)
# print(NUMANTS)
# print(EU_time_tot)
if EU_time_tot.shape[0]==0:
    print("ERROR: No Data")
    exit()

#-----make distinction for NUMANTS-----
print("WARNING SPEEDUP: Currently assumes total time to be parallelizable")
max_ants = int(NUMANTS.max())
nr_ants = np.linspace(1, max_ants, max_ants )
for ants in nr_ants:

    #-----calculate mean for each nr_thread-----
    num_threads=[] #->x_vals
    times=[] #->y_vals, matrix-> times for each nrthreads
    max_nums= int(EU_THREADS.max())
    nums = np.linspace(1, max_nums, max_nums )
    for num in nums:
        y_num=[]
        for i, j, k in zip(EU_THREADS, EU_time_tot, NUMANTS):
            if (i == num and k == ants):
                y_num.append(j)
        if len(y_num)!=0:
            num_threads.append(num)
            times.append(np.asarray(y_num))

    if(len(num_threads)==0):
        continue
    # print(num_threads)
    # print(time_tot)

    #-----calculate speedup for each nr_thread----- S_p = T_1 / T_p
    speedups=[]
    opt_speedups=[] # = T_p = T_1 / p
    t_1=0
    if num_threads[0]!=1:
        print("ERROR "+str(int(ants))+" ants: No Data For Single Thread")
        continue
    else :
        t_1=times[0].mean()

    for nt, ts in zip(num_threads ,times):
        thr_speedups=[]
        for t in ts:
            thr_speedups.append(t_1/t)
        speedups.append(thr_speedups)
        opt_speedups.append(t_1/(t_1/nt))


    #-----plot-----
    width=0.2
    if(len(num_threads)!=0):
        print("Plotting for NUMANTS="+str(int(ants)))
        plt.plot(num_threads, opt_speedups, color='red', marker='o', linewidth=3, label="Optimal Speedup")
        plt.boxplot(speedups)
        # plt.plot(num_threads, speedups, marker='o', linewidth=3, label="Speedup Ants: "+str(int(ants)))



    #-----set description-----
    plt.title(file+" (EULER) "+str(int(ants))+" ants")
    plt.xlabel("# Threads")
    plt.xticks(ticks=num_threads, labels=num_threads)
    plt.ylabel("Speedup T_1 / T_p")
    # plt.yscale('log')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.tight_layout()


    plt.savefig("Test_Cases/Plots/nr_threads__speedup_box_"+file+"_"+str(int(ants))+"ants."+img_type)
    plt.close()