from matplotlib import pyplot as plt
import pandas as pd
import sys

file = sys.argv[1]
img_type = sys.argv[2]

data = pd.read_csv('Test_Cases/'+file+'.csv')

#-----import data----- (no difference for numants & iterations)
EU_time_initialization = data.filter(like='Euler', axis=0)['time_initialization']
EU_time_main_loop = data.filter(like='Euler', axis=0)['time_main_loop']
AU_time_initialization = data.filter(like='Aurelio', axis=0)['time_initialization']
AU_time_main_loop = data.filter(like='Aurelio', axis=0)['time_main_loop']
DO_time_initialization = data.filter(like='Dominik', axis=0)['time_initialization']
DO_time_main_loop = data.filter(like='Dominik', axis=0)['time_main_loop']
JA_time_initialization = data.filter(like='Jan', axis=0)['time_initialization']
JA_time_main_loop = data.filter(like='Jan', axis=0)['time_main_loop']
MA_time_initialization = data.filter(like='Marc', axis=0)['time_initialization']
MA_time_main_loop = data.filter(like='Marc', axis=0)['time_main_loop']


#-----calculations mean & std-----
EU_time_initialization_mean = EU_time_initialization.mean()
EU_time_main_loop_mean = EU_time_main_loop.mean()
AU_time_initialization_mean = AU_time_initialization.mean()
AU_time_main_loop_mean = AU_time_main_loop.mean()
DO_time_initialization_mean = DO_time_initialization.mean()
DO_time_main_loop_mean = DO_time_main_loop.mean()
JA_time_initialization_mean = JA_time_initialization.mean()
JA_time_main_loop_mean = JA_time_main_loop.mean()
MA_time_initialization_mean = MA_time_initialization.mean()
MA_time_main_loop_mean = MA_time_main_loop.mean()



EU_time_initialization_std = EU_time_initialization.std()
EU_time_main_loop_std = EU_time_main_loop.std()
AU_time_initialization_std = AU_time_initialization.std()
AU_time_main_loop_std = AU_time_main_loop.std()
DO_time_initialization_std = DO_time_initialization.std()
DO_time_main_loop_std = DO_time_main_loop.std()
JA_time_initialization_std = JA_time_initialization.std()
JA_time_main_loop_std = JA_time_main_loop.std()
MA_time_initialization_std = MA_time_initialization.std()
MA_time_main_loop_std = MA_time_main_loop.std()

#-----plot-----
width=0.2
color_seq='#a9eaea'
color_crit='#eec900'
plt.bar('Euler', EU_time_initialization_mean, width, yerr=EU_time_initialization_std, color=color_seq, label="time_initialization")
plt.bar('Euler', EU_time_main_loop_mean, width, bottom=EU_time_initialization_mean, yerr=EU_time_main_loop_std, color=color_crit, label="time_main_loop")
plt.bar('Aurelio', AU_time_initialization_mean, width, yerr=AU_time_initialization_std, color=color_seq, label="time_initialization")
plt.bar('Aurelio', AU_time_main_loop_mean, width, bottom=AU_time_initialization_mean, yerr=AU_time_main_loop_std, color=color_crit, label="time_main_loop")
plt.bar('Dominik', DO_time_initialization_mean, width, yerr=DO_time_initialization_std, color=color_seq, label="time_initialization")
plt.bar('Dominik', DO_time_main_loop_mean, width, bottom=DO_time_initialization_mean, yerr=DO_time_main_loop_std, color=color_crit, label="time_main_loop")
plt.bar('Jan', JA_time_initialization_mean, width, yerr=JA_time_initialization_std, color=color_seq, label="time_initialization")
plt.bar('Jan', JA_time_main_loop_mean, width, bottom=JA_time_initialization_mean, yerr=JA_time_main_loop_std, color=color_crit, label="time_main_loop")
plt.bar('Marc', MA_time_initialization_mean, width, yerr=MA_time_initialization_std, color=color_seq, label="time_initialization")
plt.bar('Marc', MA_time_main_loop_mean, width, bottom=MA_time_initialization_mean, yerr=MA_time_main_loop_std, color=color_crit, label="time_main_loop")



#-----set description-----
plt.title(file)
plt.xlabel("Machine")
plt.ylabel("Time")
# plt.yscale('log')
plt.legend(['time_initialization', 'time_main_loop'], loc='upper right')
plt.tight_layout()


plt.savefig("Test_Cases/Plots/seq_stacked_bar_machines_"+file+"."+img_type)
plt.close()