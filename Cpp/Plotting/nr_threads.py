from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

file = sys.argv[1]
img_type = sys.argv[2]

data = pd.read_csv('Test_Cases/'+file+'.csv')

#-----import data----- (no difference for numants & iterations)
EU_THREADS = data.filter(like='Euler', axis=0)['THREADS/PROCESSES']
EU_time_tot = data.filter(like='Euler', axis=0)['time_tot']
# print(EU_THREADS)
# print(EU_time_tot)
if EU_THREADS.shape[0]==0:
    print("ERROR: No Data")
    exit()

#-----calculate mean for each NUMANT-----

num_threads=[] #->x_vals
time_tot=[] #->y_vals
max_nums=EU_THREADS.max()
nums = np.linspace(1, max_nums, max_nums )
for num in nums:
    y_num=[]
    for i, j in zip(EU_THREADS, EU_time_tot):
        if i == num:
            y_num.append(j)
    if len(y_num)!=0:
        num_threads.append(num)
        time_tot.append(np.asarray(y_num).mean())

# print(num_threads)
# print(time_tot)

#-----plot-----
plt.plot(num_threads, time_tot, marker='o', linewidth=3, label="Total Time")



#-----set description-----
plt.title(file+" (EULER)")
plt.xlabel("# Threads")
plt.xticks(ticks=EU_THREADS, labels=EU_THREADS)
plt.ylabel("Time")
# plt.yscale('log')
plt.legend(loc='upper right')
plt.tight_layout()


plt.savefig("Test_Cases/Plots/nr_threads_"+file+"."+img_type)
plt.close()