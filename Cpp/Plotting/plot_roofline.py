from matplotlib import pyplot as plt

#-----set data-----
pi=2 #peak performance (ops/cycle)
beta=1 #bandwidth (ops/cycle)
measurement=[1.5,1]

x_vals = [0, 0.125, 0.25, 0.5, 1, 2, 4] #operational intensity (ops/bytes)
y_beta_bound = [] #performance (ops/cycle)
y_pi_bound = [] #performance (ops/cycle)

for i in x_vals:
    y_beta_bound.append(beta*i)
    y_pi_bound.append(pi)

#-----plotting-----
plt.plot(x_vals, y_pi_bound, color='r', linestyle='--', marker='.', label="Pi bound")
plt.plot(x_vals, y_beta_bound, color='b', marker='.', label="Beta bound")
plt.plot(measurement[0], measurement[1], color='black', marker='x', markersize=20, label="Measurement")

border=0
for i in range(len(x_vals)):
    if y_beta_bound[i]>y_pi_bound[i]:
        border=i
        break

plt.fill_between(x_vals[:border], y_beta_bound[:border], color='b', alpha=0.25)
plt.fill_between(x_vals[border-1:], y_pi_bound[border-1:], color='r', alpha=0.25)


#-----info-----
plt.legend()
plt.title("Roofline model")
plt.xlabel("Operational Intensity (Ops / Bytes)")
plt.ylabel("Performance (Ops / Cycle)")

plt.grid(True)
plt.tight_layout()

plt.savefig("Test_Cases/Plots/Roofline."+img_type)