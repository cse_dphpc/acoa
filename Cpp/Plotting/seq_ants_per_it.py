from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

file = sys.argv[1]
img_type = sys.argv[2]

data = pd.read_csv('Test_Cases/'+file+'.csv')

#-----import data----- (no difference for numants & iterations)
EU_NUMANTS = data.filter(like='Euler', axis=0)['NUMANTS']
EU_time_tot = data.filter(like='Euler', axis=0)['time_tot']
# print(EU_NUMANTS)
# print(EU_time_tot)
if EU_NUMANTS.shape[0]==0:
    print("ERROR: No Data")
    exit()

#-----calculate mean for each NUMANT-----
# print("WARNING: NUMANTS only multiple of 10 (seq_ants_per_it)")

numants=[] #->x_vals
time_tot=[] #->y_vals
max_nums=EU_NUMANTS.max()
nums = np.linspace(1, max_nums, max_nums)
for num in nums:
    y_num=[]
    for i, j in zip(EU_NUMANTS, EU_time_tot):
        if i == num:
            y_num.append(j)
    if len(y_num)!=0:
        numants.append(num)
        time_tot.append(np.asarray(y_num).mean())

# print(numants)
# print(time_tot)


#-----plot-----
plt.plot(numants, time_tot, marker='o', linewidth=3, label="Total Time")



#-----set description-----
plt.grid(True)
plt.title(file+": 5'000 Iterations (EULER)")
plt.xlabel("Ants")
plt.xticks(ticks=EU_NUMANTS, labels=EU_NUMANTS)
plt.ylabel("Time (seconds)")
# plt.yscale('log')
plt.legend(loc='upper right')
plt.tight_layout()


plt.savefig("Test_Cases/Plots/seq_ants_per_it_"+file+"."+img_type)
plt.close()