from matplotlib import pyplot as plt
import pandas as pd
import sys

file = sys.argv[1]
img_type = sys.argv[2]



data = pd.read_csv('Test_Cases/'+file+'.path')
x_vals = data['x_vals']
y_vals = data['y_vals']


plt.plot(x_vals, y_vals , marker='o', color='b', alpha=1)


plt.title("Shortest Path "+file)
plt.xlabel("x")
plt.ylabel("y")
plt.tight_layout()


plt.savefig("Test_Cases/Plots/sp_"+file+"."+img_type)
plt.close()
