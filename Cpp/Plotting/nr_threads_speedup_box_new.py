from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import sys

file = sys.argv[1]
number_of_ants = int(sys.argv[2])
img_type = sys.argv[3]
# platforms=['Euler_MPI', 'Euler_GroupThread', 'Euler_OMP']
platforms=['Euler_MPI']
colors=['g', 'm', 'b']

data = pd.read_csv('Test_Cases/'+file+'.csv')

#*****2.1 optimal speedup***** anzupassen fuer Modell

#-----read data-----
EU_Master_time_seq = data.filter(like='Euler_Master', axis=0)['time_initialization']
EU_Master_time_prl = data.filter(like='Euler_Master', axis=0)['time_main_loop']
EU_Master_NUMANTS= data.filter(like='Euler_Master', axis=0)['NUMANTS']

def filter_by_numants(data_vec, numants_vec, numants):
    filtered_data=[]
    for i,j in zip(data_vec, numants_vec):
        if j==numants:
            filtered_data.append(i)
    return filtered_data
EU_Master_time_seq = np.asarray(filter_by_numants(EU_Master_time_seq,EU_Master_NUMANTS, number_of_ants))
EU_Master_time_prl = np.asarray(filter_by_numants(EU_Master_time_prl,EU_Master_NUMANTS, number_of_ants))

#------avg data-----
time_seq = EU_Master_time_seq.mean()
time_prl = EU_Master_time_prl.mean()
T1=time_seq+time_prl



#*****1.measurements*****

#-----import data----- (no difference for numants & iterations)
EU_THREADS_all=[] #list of vectors
EU_time_tot_all=[] #list of vectors
NUMANTS_all=[] #list of vectors
for platform in platforms:
    EU_THREADS= data.filter(like=platform, axis=0)['THREADS/PROCESSES']
    EU_THREADS_all.append(EU_THREADS)
    NUMANTS = data.filter(like=platform, axis=0)['NUMANTS']
    NUMANTS_all.append(NUMANTS)
    EU_time_tot = data.filter(like=platform, axis=0)['time_tot']
    EU_time_tot_all.append(EU_time_tot)

#-----filter data for NUMANTS=number_of_ants-----
# def filter_by_numants(data_vec, numants_vec, numants):
#     filtered_data=[]
#     for i,j in zip(data_vec, numants_vec):
#         if j==numants:
#             filtered_data.append(i)
#     return filtered_data

for i in range(len(EU_THREADS_all)): #for each platform
    EU_THREADS_all[i] = np.asarray(filter_by_numants(EU_THREADS_all[i], NUMANTS_all[i], number_of_ants))
    EU_time_tot_all[i] = np.asarray(filter_by_numants(EU_time_tot_all[i], NUMANTS_all[i], number_of_ants))

# #-----sort by nr threads & get speedups-----
def filter_by_nrthreads(data_vec, threads_vec, nrthreads):
    filtered_data=[]
    for i,j in zip(data_vec, threads_vec):
        if j==nrthreads:
            filtered_data.append(i)
    return filtered_data

times_all=[] #here not vector each, but vector of vector
speedups_all=[] #here not vector each, but vector of vector
threads_all=[]
for i in range(len(EU_THREADS_all)): #for each platform
    times=[]
    threads=[]
    max_threads = int(EU_THREADS_all[i].max())
    for thr in range(max_threads):
        filtered=np.asarray(filter_by_nrthreads(EU_time_tot_all[i], EU_THREADS_all[i], thr+1))
        times.append(np.asarray(filtered))
        speedups_thr=[]
        # for t in times:
        #     speedups_thr.append(T1/t)
        # speedups.append(np.asarray(speedups_thr))
        threads.append(thr+1)
        # print(str(filtered)+","+str(thr+1))
    # print("*")
    # print(times)
    times_all.append(times)

    speedups=[]
    for timevec in times:
        speedup_vec=[]
        for t in timevec:
            speedup_vec.append(T1/t)
        speedups.append(np.asarray(speedup_vec))
    speedups_all.append(speedups)

    threads_all.append(threads)


# -----PLOTTING-----
for i in range(len(threads_all)): #for each platform
    print("Plotting for platform="+str(platforms[i]))
    # plt.plot(threads_all[i], speedups_all[i], marker='o', color=colors[i], linewidth=3, label=platforms[i])
    plt.boxplot(speedups_all[i])


#*****2.2 optimal speedup***** anzupassen fuer Modell
#-----times to optimal speedup-----
def optimal_speedup(time_seq, time_prl, num_threads):
    speedups=[]
    for tr in num_threads:
        time = time_seq+time_prl/tr
        speedups.append((time_seq+time_prl)/time)
    return speedups

max_threads=[]
for i in threads_all:
    max_threads.append(np.asarray(i).max())
max_threads=np.asarray(max_threads).max()
opt_threads=np.linspace(1, max_threads, max_threads)
opt_speedups = optimal_speedup(time_seq, time_prl, opt_threads)

#-----plotting-----
print("Plotting Optimal SPUP")
plt.plot(opt_threads, opt_speedups, linestyle='--', marker='o', color='r', linewidth=3, label='Optimal')


#*****3.information*****
plt.plot([], [], linestyle='', marker='', color='w', linewidth=3, label=' ')
plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='alpha=1, beta=3')
plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='R=0.05, Q=10')
plt.plot([], [], linestyle='', marker='>', color='black', linewidth=3, label='Iterations=5000')


#-----set description-----
plt.title(file+" Speedup ("+str(number_of_ants)+" ants)")
plt.xlabel("# Threads/Processes")
# plt.xticks(ticks=num_threads, labels=num_threads)
plt.ylabel("Speedup T_1 / T_p")
# plt.yscale('log')
plt.legend(loc='upper left')
plt.grid(True)
plt.tight_layout()

plt.savefig("Test_Cases/Plots/nr_threads__speedup_box_"+file+"_"+str(int(number_of_ants))+"ants."+img_type)
plt.close()